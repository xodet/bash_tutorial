#!/bin/bash
. functions.sh

sample1() {
	title "Wykorzystanie podpowlok"

	infonl "Przypisanie wyniku do zmiennej:"
	A=$(uname -a)
	labelnl "Wersja linuxa:" "$A"
	pause


	infonl "Listowanie plikow:"
	for file in `ls *.sh`; do
		lines=$(cat "$file" | wc -l)
		labelnl "$file" "-> $lines"
	done
	pause
}

sample2() {
	title "Wplyw podpowlok na kontekst"

	infonl "Widocznosc zmiennych:"
	OUTER="qwe"
	(ENV="123"; echo ">>" $ENV $OUTER)
	echo ">>" $ENV $OUTER
	pause


	infonl "Wplyw na zewnetrzna powloke:"
	printf "tutaj: `pwd`\ntam:   `cd ~; pwd`\ntutaj: `pwd`\n"
	pause


	infonl "Przekierowywanie strumienia z podpowloki:"
	(ls wtf; pwd) &2> tempfile.txt
	labelnl "Tresc:"
	cat tempfile.txt
	rm tempfile.txt
	pause
}

sample3() {
	title "Koszt uzytkowania"

	info "Normalnie: "
	tBegin=`date +%s%3N`
	for((i=0;i<1000;i++)) {
		cd /var
		cd ~
	}
	echo $[ `date +%s%3N` - $tBegin ]ms
	pause
	

	info "Subshelle: "
	tBegin=`date +%s%3N`
	for((i=0;i<1000;i++)) {
		(
			cd /var
			cd ~
		)
	}
	echo $[ `date +%s%3N` - $tBegin ]ms
	pause
}

sample1
sample2
sample3