#!/bin/bash
. functions.sh

sample1() {
	title "Manipulowanie trescia stdout"

	cat xarg/example.txt
	pausenl

	cat xarg/example.txt | xargs
	pause

	cat xarg/example.txt | xargs -n 3
	pause

	echo "1;2;3;4;5;6" | xargs -d ";" -n 2
	pause
}

sample2() {
	title "Praktyczne wykorzystanie"

	cat xarg/example.txt | xargs -n 3 ./xarg/echo_helper.sh
	pause

	cat xarg/example.txt | \
		xargs -n 3 | \
		xargs -I {} ./xarg/echo_helper.sh -foo {} -bar
	pause
}

sample1
sample2