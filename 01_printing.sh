#!/bin/bash
. functions.sh

sample1() {
	title "Tekst za pomoca 'echo'"

	echo 'ab'cdef
	
	echo "ghijkl" 123
	
	echo -n "mno"pqr"stw"
	
	pause
}

sample2() {
	title "Tekst za pomoca 'printf'"
	
	printf "%s - %s\n\n" "A" "B"
	
	printf "%x|%3d" 105 3
	
	pausenl
}

sample3() {
	title "Przeglad dostepnych modyfikacji czcionki"
	
	colorIds="$(seq 0 10) $(seq 40 47) $(seq 90 107)"
	for B in $colorIds; do
		for A in $(seq 0 1); do
			printf "\e[%s;%sm SAMPLE \e[0m " $A $B
		done
	printf "= %3s\n" $B
	done
	
  pause
}

sample4() {
	title "Efekty mozna ze soba laczyc - wszyskie czcionki zolte"
	
	colorIds="$(seq 0 10) $(seq 40 47) $(seq 90 107)"
	for B in $colorIds; do
		for A in $(seq 0 1); do
			printf "\e[%s;93m\e[%s;%sm SAMPLE \e[0m " $B $A $B
		done
		printf "= %3s\n" $B
	done
	
	pause
}

sample5() {
	title "Zbiorczy przyklad"

	podkreslenie="\e[0;4m"
	czerwonaCzcionka="\e[1;91m"
	szareTlo="\e[1;100m"
	wyzerowanie="\e[0m"

	printf "\n${podkreslenie}${czerwonaCzcionka}${szareTlo}Czerwony, podkreslony tekst na szarym tle.${wyzerowanie}"

	pausenl
}

sample1
sample2
sample3
sample4
sample5

# http://stackoverflow.com/questions/2188199/how-to-use-double-or-single-bracket-parentheses-curly-braces
# http://askubuntu.com/questions/606378/when-to-use-vs-in-bash