#!/bin/bash
. functions.sh

sample1() {
	title "Mozliwosci wykorzystania pipeow"

	info "Linii w plikach: "
	ls *.sh | while read filename; do
		cat "./$filename"
	done | wc -l
	pause


	# puste linie by nie psuc zabawy ;)



































	# !!! kto potrafi skrocic powyzszy zapis?
	info "Skrocona wersja: "
	cat *.sh | wc -l
	pause
	# a czemu nie "wc -l *sh" ?


	info "Linii min. 10 znakow: "
	cat *.sh | while read line; do
		if [ ${#line} -ge 10 ]; then
			echo $line
		fi
	done | wc -l
	pause


	wc -l *.sh | (
		sort -n -k 1 | head -n 5
		echo "  777 fake.file.txt"
	) | sort -k 2 -r
	pause
}

sample2() {
	title "Kod bledu przy laczeniu strumieni"

	ls omg
	labelnl "Kod bledu:" "$?"
	pause


	ls omg | wc -l &> /dev/null
	labelnl "Kod bledu:" "$?"
	pause
}

sample1
sample2