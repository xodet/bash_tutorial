#!/bin/bash
. functions.sh

sample1() {
	title "Klasyczna tablica"

	tab=("q w" e r t)

	for((i = 0; i < ${#tab[*]}; i++)) {
		printf "${tab[$i]}, "
	}
	pausenl


	label "Elementy: "
	echo ${tab[*]} " |LUB| " ${tab[@]}

	label "Ilosc:    "
	echo ${#tab[*]} " |LUB| " ${#tab[@]}

	label "Indeksy:  "
	echo ${!tab[*]} " |LUB| " ${!tab[@]}
	pause


	label "Pierwszy element: "
	echo ${tab[0]} " |LUB| " $tab
	pause


	info "\nUwaga na listowanie elementow!:"
	pause
	labelnl "\n* - unquoted:"
	for i in ${tab[*]}; do
		echo "$i, "
	done
	labelnl "\n@ - unquoted:"
	for i in ${tab[@]}; do
		echo "$i, "
	done
	pause

	labelnl "\n* - quoted:"
	for i in "${tab[*]}"; do
		echo "$i, "
	done
	labelnl "\n@ - quoted:"
	for i in "${tab[@]}"; do
		echo "$i, "
	done
	pause
}

sample2() {
	title "Tablice asocjacyjne"

	declare -A tabasc
	tabasc=([q]="first value" [w]= [e]=third [r]=5 [t]='sixth')

	for i in ${!tabasc[*]}; do
		printf "${tabasc[$i]}, "
	done
	pausenl


	label "Elementy: "
	echo ${tabasc[*]} " |LUB| " ${tabasc[@]}

	label "Ilosc:    "
	echo ${#tabasc[*]} " |LUB| " ${#tabasc[@]}

	label "Indeksy:  "
	echo ${!tabasc[*]} " |LUB| " ${!tabasc[@]}
	pause


	label "Pierwszy element: "
	firstIndex=`echo ${!tabasc[*]} | tr ' ' '\n' | head -n 1`
	echo ${tabasc[$firstIndex]}
	pause
}

sample3() {
	title "\"Tablica\" iteracyjna (szereg)"

	tabseq="q w e r t"

	for i in $tabseq; do
		printf "$i, "
	done
	pausenl


	label "Elementy: "
	echo ${tabseq[*]}

	label "Ilosc:    "
	echo ${#tabseq[*]}

	label "Indeksy:  "
	echo ${!tabseq[*]} " |LUB| " ${!tabseq[@]}
	pause


	label "Pierwszy element: "
	echo ${tabseq[0]}
	pause
}

sample4() {
	title "Konwersje i operacje"

	info "\nLista na tablice:\n"
	list=`ls *.sh`
	tab=($list)

	label "Elementy: "
	echo ${tab[*]} | sed "s/ /, /g"
	label "Ilosc:    "
	echo ${#tab[*]}
	pause


	info "\nTablica asocjacyjna na tablice:\n"
	declare -A tabasc
	tabasc=([a]="first" [b]="second" [c]="third")
	tab=(${tabasc[*]})

	label "Elementy: "
	echo ${tab[*]} | sed "s/ /, /g"
	label "Ilosc:    "
	echo ${#tab[*]}
	pause


	info "\nUsuwanie elementu:\n"
	tab=(q w e r t)
	unset tab[2]

	label "Elementy: "
	echo ${tab[*]}
	label "Ilosc:    "
	echo ${#tab[*]}
	label "Indeksy:  "
	echo ${!tab[*]}
	pause


	info "\nDodawanie elementu:\n"
	tab=(q w e r t)

	tab[50]=f
	tab[10]=g

	label "Elementy: "
	echo ${tab[*]}
	label "Ilosc:    "
	echo ${#tab[*]}
	label "Indeksy:  "
	echo ${!tab[*]}
	pause
}

sample1
sample2
sample3
sample4