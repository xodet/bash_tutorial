#!/bin/bash
. functions.sh

sample1() {
	title "Wypisywanie zmiennych"
	
	varA="ABC"
	varB="123"

	echo "$varA$varB"
	echo "$varAtekst$varB"
	echo "$varA tekst$varB"
	echo "${varA}tekst$varB"
	echo "$varA""tekst$varB"
	echo  $varA"tekst$varB"
	pause
}


sample2() {
	title "Widocznosc zmiennych"

	globalVar=10
	echo "[ $globalVar / $newVar / $localVar ]"
	pause

	changeValues

	echo "[ $globalVar / $newVar / $localVar ]"
	pause
}

changeValues() {
	globalVar=20
	newVar="hey"
	local localVar="Zmienna lokalna"

	childFunction
}

childFunction() {
	local globalVar=30

	echo "[ $globalVar / $newVar / $localVar ]"
	pause
}

sample3() {
	title "Operacje arytmetyczne na zmiennych"

	varA=2
	let varB="8"

	out1=varA*varB
	let out2=varA*varB
	out3=$[ $varA * varB ]
	out4=$(( varA * varB ))
	out5=`expr $varA \* $varB`
	out6=`echo "sqrt($varA * $varB)" | bc`

	printf "$out1\n$out2\n$out3\n$out4\n$out5\n$out6\n"
	pause
}

sample4() {
  title "Zmienne srodowiskowe"

  infonl "Lista ostatnich 10ciu zmiennych:"
  # $$ lub $BASHPID to processid aktualnego procesu
  sudo cat /proc/$$/environ | tr "\0" "\n" | tail -n 10
  pause

  label "Ilosc wszystkich zmiennych srodowiskowych: "
  sudo cat /proc/$$/environ | tr "\0" "\n" | wc -l
  pause
}

sample5() {
	title "Eksport zmiennych srodowiskowych"
	infonl "Jest to trudny temat i powinien zostac omowiony pozniej!"
	pause

	export EKSPORTOWA_ZMIENNA="przykladowa_wartosc"

	# tutaj zmiennej nie bedzie, bo nie byla "uruchowmieniowa"
	label "Ilosc wystapien zmiennej w aktualnym procesie: "
	sudo cat /proc/$$/environ | tr "\0" "\n" | grep ZMIENNA | wc -l
	pause

	# tymczasowy notatnik
	labelnl "Wyszukana zmienna w zespawnowanym procesie:"
	spawnLeafpad
	pause

	# tymczasowy notatnik po zmianach
	EKSPORTOWA_ZMIENNA="nadpisana_wartosc"
	LOKALNA_ZMIENNA="lokalna_wartosc"
	labelnl "Zmieniona zmienna w zespawnowanym procesie:"
	spawnLeafpad
	pause
}

spawnLeafpad() {
	(leafpad& 2> /dev/null)
	sudo cat /proc/`pgrep leafpad`/environ | tr "\0" "\n" | grep ZMIENNA
	killall -q leafpad
}

sample1
sample2
sample3
sample4
sample5