#!/bin/bash
. functions.sh

sample1() {
	title "Zapis strumienia wyjsciowego"

	# dodac nazwy strumioeni
	echo "123" > wynik.txt
	cat "wynik.txt"
	pause

	echo "456" >> wynik.txt
	cat "wynik.txt"
	pause

	echo "789" 1> wynik.txt
	cat "wynik.txt"
	pause

	#clean
	rm wynik.txt
}

sample2() {
	title "Strumien wejsciowy"

	echo "881 + 456" > wynik.txt
	bc < wynik.txt
	pause

	#clean
	rm wynik.txt
}

sample3() {
	title "Rodzaje strumieni i ich zapis"

	touch someFile

	ls fakeFile someFile
	pausenl

	ls fakeFile someFile 2> wynik.txt
	infonl "Odczyt z pliku: "
	cat "wynik.txt"
	pausenl

	ls fakeFile someFile &> wynik.txt
	infonl "Odczyt z pliku:"
	cat "wynik.txt"
	pause

	#clean
	rm wynik.txt someFile
}

sample4() {
	title "Wlasne strumienie - deskryptory"

	exec 3> newFile.txt
	echo "testowa tresc" >&3
	info "Odczyt z pliku: "
	cat newFile.txt
	pausenl

	cat <&3
	pausenl

	exec 4< newFile.txt
	info "Odczyt z pliku: "
	cat <&4
	pause

	#clean - unsuccesfull
	rm newFile.txt
	pause

	exec 4<&-
	exec 3>&-

	#clean
	rm newFile.txt
}

sample1
sample2
sample3
sample4