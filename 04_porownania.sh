#!/bin/bash
. functions.sh

sample1() {
	title "Podstawowe porownanie"

	A=1
	B=2

	if [ $A = $B ]
	then
		echo "pierwszy"
	elif [ $A = 1 ]
	then
		echo "drugi"
	else
		echo "trzeci"
	fi
	pause


	if [ ! 1 = 1 ]; then	#spacja ma znaczenie!
		echo "prawda"
	else
		echo "falsz"
	fi
	pause


	if test 1 = 1; then
		echo "to tez dziala"
	fi
	pause
}

sample2() {
	title "Warunkowa akcja na skroty"

	tmpA=""
	[ 1 = 1 ] && tmpA="test"
	echo $tmpA
	pause

	# to samo co powyzej
	# if [ 1 = 1 ]; then
	# 	tmpA="test"
	# fi


	[ 1 = 2 ] || tmpA="test2"
	echo $tmpA
	pause
}

sample3() {
	title "Porownania matematyczne"

	A="1"
	B="xyz"

	[ $A -eq 1 ] && echo "takie same" || echo "rozne"
	#dostepne rowniez:
	# -gt, -lt, -ge, -le, -ne
	pause


	[ $B -eq "xyz" ] && echo "takie same" || echo "rozne"
	pause
}

sample4() {
	title "Zabezpieczenie porownywanej wartosci"

	A="1 2"

	[ $A = "1 2" ] && echo "takie same" || echo "rozne"
	pause

	[ "$A" = "1 2" ] && echo "takie same" || echo "rozne"
	pause
}

sample5() {
	title "Laczenie warunkow"

	A=1
	B=2
	C=2

	if [ $A = $C ] || [ $A = $B -o $B = $C ]
	then
		echo "przeszlo"
	else
		echo "cos nie pyklo"
	fi
	pause
}

sample6() {
	title "Pulapki"

	if [ 1=2 ]; then
		echo "prawda"
	else
		echo "falsz"
	fi
	pause


	if [ false ]; then
		echo "jeden"
	elif [ true ]; then
		echo "dwa"
	else
		echo "trzy"
	fi
	pause


	#prawidlowo zapisany powyzszy przyklad
	if false; then
		echo "jeden"
	elif true; then
		echo "dwa"
	else
		echo "trzy"
	fi
	pause
}


sample7() {
	title "Testowanie"

	plik="plik.txt"
	touch $plik

	[ -f $plik ] && echo "plik" || echo "cos innego"
	pause

	[ -e $plik ] && echo "isnieje" || echo "nie istnieje"
	pause

	unlink $plik
}

sample8() {
	title "Porownania tekstowe"

	A=123
	B=234567
	C=""

	if [[ $A = "123" ]] && [ $A == "123" ]; then	# ogarnac co tu sie stalo. czemu w te strone.
		echo "rowne"
	else
		echo "inne"
	fi
	pause


	[[ $A > $B ]] && echo "tak" || echo "nie"
	pause


	[[ $A != $B ]] && echo "tak" || echo "nie"
	pause

	
	if [[ -z $C ]] && [[ -n $A ]]; then
		echo "pusty i niepusty"
	fi
	pause
}

sample9() {
	title "Switch"

	var="hello"

	case "$var" in
		123)
			echo "tak"
			;;
		[Hh]ello)
			echo "albo tak"
			;;
		*)
			echo "nijak"
			;;
	esac
}

sample1
sample2
sample3
sample4
sample5
sample6
sample7
sample8
sample9