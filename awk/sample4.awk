#!/usr/bin/awk -f

function maxNumber(array)
{
	result = 0;
	for (i in array) {
		if (array[i] > result) {
			result = array[i]
		}
	}

	return result
}

( $NF != "total" ) {
	lines[$2]=$1
}

END {
	max = maxNumber(lines)
	for (i in lines) {
		scaled = 30 * lines[i] / max
		printf "%-17s [%4d]: ", i, lines[i]

		while(scaled-- > 0) {
			printf "#"
		}
		printf "\n"
	}
}