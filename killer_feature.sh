#!/bin/bash
# === consts section ===
declare -A color
color=([RED]='\e[0;31m' [GREEN]='\e[0;32m' [YELLOW]='\e[0;33m' [BLUE]='\e[1;34m' [PURPLE]='\e[0;35m' [CYAN]='\e[0;36m' [WHITE]='\e[0;37m' [IBLACK]='\e[0;90m' [IRED]='\e[0;91m' [IGREEN]='\e[0;92m' [IYELLOW]='\e[0;93m' [IBLUE]='\e[0;94m' [IPURPLE]='\e[0;95m' [ICYAN]='\e[0;96m' [IWHITE]='\e[0;97m' [RESET]='\e[0m')

declare -A box
box=([TOP_LEFT]=6c [HORIZONTAL]=71 [TOP_RIGHT]=6b [VERTICAL]=78 [EMPTY]=20 [BOTTOM_LEFT]=6d [BOTTOM_RIGHT]=6a)

declare -A snake
snake=([HEAD]='O' [BODY]='#' [FOOD]='$')

WIDTH=`tput cols`			# number of columns in terminal
HEIGHT=`tput lines`			# number of rows in terminal
let WIDTH_SEQ=$WIDTH-1		# useful variable in loops
let HEIGHT_SEQ=$HEIGHT-1	# useful variable in loops

WALK_THROUGH_WALLS=true
# === consts section end ===

play() {
	while(true); do	# game loop
		initialize
		moveX=1; moveY=0
		while(true); do # play loop
			for wait in `seq 0 2`; do
				read -sn 1 -t 0.01 key
				case "$key" in
					[wW])
						if [ $moveY = 0 ]; then
							moveX=0; moveY=-1
						fi;;
					[sS])
						if [ $moveY = 0 ]; then
							moveX=0; moveY=1
						fi;;
					[aA])
						if [ $moveX = 0 ]; then
							moveX=-1; moveY=0
						fi;;
					[dD])
						if [ $moveX = 0 ]; then
							moveX=1; moveY=0
						fi;;
				esac
			done

			moveSnake $moveX $moveY
			if [ $? -eq 1 ]; then
				endScreen
				break
			fi
		done	# end play loop
	done	#end game loop
}

initialize() {
	clear
	stty -echo	# turn off text written in console by user
	tput civis	# hide cursor

	drawOuterBox
	prepareSnake
	spawnFood
}

drawOuterBox() {
	setColor IRED
	for y in `seq 0 $HEIGHT_SEQ`; do
		if [ $y -eq 0 ] || [ $y -eq $HEIGHT_SEQ ]; then
			for x in `seq 0 $WIDTH_SEQ`; do
				getBoxChar HORIZONTAL
			done
		else
			getBoxChar VERTICAL
			printf "%*s" $((WIDTH - 2))
			getBoxChar VERTICAL
		fi
		if [ $y -ne $HEIGHT_SEQ ]; then
			printf "\n"
		fi
	done

	setPos 0 0
	getBoxChar TOP_LEFT

	setPos $WIDTH_SEQ 0
	getBoxChar TOP_RIGHT

	setPos 0 $HEIGHT_SEQ
	getBoxChar BOTTOM_LEFT

	setPos $WIDTH_SEQ $HEIGHT_SEQ
	getBoxChar BOTTOM_RIGHT
}

prepareSnake() {
	unset sX	# remove existing tables of snake positions
	unset sY

	score=0
	startWidth=10
	snakeYStart=$(( $HEIGHT / 2 ))
	snakeXStart=$(( $WIDTH / 2 - $startWidth ))

	for ((i=0; i<$startWidth; i++)); do
		sX[$i]=$(( $snakeXStart + $i ))
		sY[$i]=$snakeYStart
	done

	setPos $snakeXStart $snakeYStart
	setColor IYELLOW
	printf "%*s%s" $startWidth ${snake[HEAD]} | tr ' ' ${snake[BODY]}
	resetColorAndPos
}

moveSnake() {
	local moveX=$1
	local moveY=$2

	if $WALK_THROUGH_WALLS; then
		nextX=$(( (${sX[-1]} + $moveX - 1 + $WIDTH - 2) % ($WIDTH - 2) + 1 ))
		nextY=$(( (${sY[-1]} + $moveY - 1 + $HEIGHT - 2) % ($HEIGHT - 2) + 1 ))
	else
		nextX=$(( ${sX[-1]} + $moveX ))
		nextY=$(( ${sY[-1]} + $moveY ))
	fi

	detectColision $nextX $nextY
	if [ $? -eq 0 ]; then
		detectEatFood $nextX $nextY
		if [ $? -eq 0 ]; then
			updateSnake $nextX $nextY false
		else
			score=`expr $score + 1`
			updateSnake $nextX $nextY true
			spawnFood
		fi

		return 0
	fi

	return 1
}

updateSnake() {
	local nextX=$1
	local nextY=$2
	local foodEaten=$3

	snakeLength=${#sX[*]}

	sX[$snakeLength]=$nextX
	sY[$snakeLength]=$nextY

	setColor IYELLOW
	setPos ${sX[-2]} ${sY[-2]}
	printf ${snake[BODY]}

	setPos ${sX[-1]} ${sY[-1]}
	printf ${snake[HEAD]}

	if [ $foodEaten = false ]; then
		setPos ${sX[0]} ${sY[0]}
		getBoxChar EMPTY

		unset sX[0]
		unset sY[0]

		sX=(${sX[@]}) # rewrite (copy) table, as unsetted values
		sY=(${sY[@]}) # exist but are empty in oryginal table!
	fi

	resetColorAndPos
}

spawnFood() {
	while(true); do
		foodX=$(( RANDOM % ($WIDTH - 2) + 1 ))
		foodY=$(( RANDOM % ($HEIGHT - 2) + 1 ))
		
		detectColision $foodX $foodY
		if [ $? -eq 0 ]; then
			setPos $foodX $foodY

			setColor IGREEN
			printf ${snake[FOOD]}
			resetColorAndPos
			break;
		fi
	done
}

detectColision() {	# 0 - no colision, 1 - colision
	local nextX=$1
	local nextY=$2

	if [ $nextX = 0 ] || [ $nextX = $WIDTH_SEQ ] || [ $nextY = 0 ] || [ $nextY = $HEIGHT_SEQ ]; then
		return 1
	fi

	for i in `seq 0 ${#sX[*]}`; do
		if [[ $nextX == ${sX[$i]} ]] && [[ $nextY == ${sY[$i]} ]]; then
			return 2
		fi
	done

	return 0
}

detectEatFood() {	# 0 - no eat, 1 - eat (collision)
	local nextX=$1
	local nextY=$2

	if [ $nextX -eq $foodX ] && [ $nextY -eq $foodY ]; then
		return 1
	fi

	return 0
}

endScreen() {
	message="Twoj wynik to: $score"
	length=${#message}

	messageY=$(( $HEIGHT / 3 ))
	messageX=$(( ($WIDTH - $length) / 2 ))

	setPos $messageX $messageY

	setColor IPURPLE
	echo $message
	resetColorAndPos

	read -n 1
}

setPos() {
	tput cup $2 $1	# set cursor on position Y X
}

resetPos() {
	setPos $WIDTH_SEQ $HEIGHT_SEQ
}

getBoxChar() {
	printf "\e(0\x${box[$1]}\e(B"
}

setColor() {
	printf ${color[$1]}
}

resetColor() {
	setColor RESET
}

resetColorAndPos() {
	resetColor
	resetPos
}

play