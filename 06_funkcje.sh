#!/bin/bash
. functions.sh

title "Informacje o wywolanym skrypcie"

label "Ilosc argumentow:    "; echo "$#"
label "Zerowy argument:     "; echo "$0"
label "Pierwszy argument:   "; echo "$1"
label "Wszystkie argumenty: "; echo "$@"
pause

sampleError() {
	# nigdy nie parsowane przez Basha
	wegfwegwg
	wregvare
	./wegfwegwg
	`w4efwef`
}

sample1() {
	title "Funkcja wywolana z argumentami"

	sample_sub A B C

	sample_sub "D E F"

	VAR="G H 
	I J K"
	
	sample_sub $VAR
	
	sample_sub "$VAR"
}

function sample_sub() {
	label "Ilosc argumentow:    "; echo "$#"
	label "Zerowy argument:     "; echo "$0"
	label "Pierwszy argument:   "; echo "$1"
	label "Wszystkie argumenty: "; echo "$@"
	pause
}

sample1