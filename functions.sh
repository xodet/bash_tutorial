#!/bin/bash

title() {
	clear
	ch="="
	printf "\e[1;94m%s\r%s \e[1;93m%s \e[0m\n" $(printf "%*s" "60" | tr " " "$ch") "$ch$ch$ch" "$1"
	pause
}

colorText() {
	printf "\e[0;$1m$2\e[0m"
	if [ $# -gt 2 ] && [ -n "$3" ]; then
		printf " $3"
	fi
}

info() {
	colorText "92" "$1" "$2"
}

infonl() {
	info "$1" "$2\n"
}

label() {
	colorText "93" "$1" "$2"
}

labelnl() {
	label "$1" "$2\n"
}

pause() {
	read -n 1
}

pausenl() {
	pause
	printf "\n"
}