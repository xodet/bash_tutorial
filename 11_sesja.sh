#!/bin/bash
. functions.sh

sample1() {
	title "Wielowatkowosc 1"

	simpleLoop A &
	simpleLoop B &
	simpleLoop C
	pause
}

simpleLoop() {
	for i in {1..10}; do
		printf "$1 "
		sleep 0.1
	done
}

sample2() {
	title "Wielowatkowosc 2"

	# session -> multiple process groups -> multiple processes -> multiple threads

	( sleep 1; touch "hello.txt" ) &
	ls *.txt
	sleep 2
	ls *.txt
	pause

	infonl "Ilosc zadan od poczatku skryptu:"
	jobs
	pause

	rm hello.txt
}

sample3() {
	title "Informacje o podprocesach"

	infonl "Zwykle wywolanie"
	myDetails

	infonl "Wywolanie w podprocesie"
	myDetails &

	sleep 1
	infonl "Podwojny podproces"
	( myDetails & ) &
	pausenl
}

sample4() {
	title "Forkowanie podprocesow"

	( sleep 5; infonl "Osobny proces:"; myDetails ) &
	infonl "Jobs:"
	jobs

	disown
	infonl "\nJobs (po disownie):\n"
	jobs

	psAuxDetails
	pause
}

sample5() {
	title "Plik tworzony w zalogowanej sesji z opoznieniem"

	ssh example@localhost <<< echo "
		echo 'Proces w SSH'
		sleep 10
		touch plik_utworzony_po_wylogowaniu.txt
		echo 'Wylogowuje z ssh'
	" &

	sleep 4

	psAuxDetails
	pause
}

sample6() {
	title "Plik tworzony w rozlaczonej sesji"

	ssh example@localhost <<< echo "
		echo 'Proces w SSH'
		sleep 10
		touch plik_utworzony_po_wylogowaniu_2.txt 
		exit
	" &
	disown

	sleep 4
	killall ssh

	psAuxDetails
	exit
}

myDetails() {
	labelnl "PID (rodzic):" "$$"
	labelnl "BashPID:     " "$BASHPID"
	labelnl "Subshell #no:" "$BASH_SUBSHELL"
	echo ""
}

psAuxDetails() {
	infonl "\nSzczegoly:"
	settings="axo pid,ppid,user,command:20"
	ps $settings | head -n 1
	ps $settings | grep sleep | grep -v grep
	echo ""
}

sample1
sample2
sample3
sample4
sample5
sample6

# http://stackoverflow.com/questions/7858191/difference-between-bash-pid-and
# http://unix.stackexchange.com/questions/28809/how-to-totally-fork-a-shell-command-that-is-using-redirection