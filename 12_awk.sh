#!/bin/bash
. functions.sh

sample1() {
	title "Podstawa"

	ls -l | awk '{print $1", "$NF}'
	pausenl
}

sample2() {
	title "Obsluga blokow \"BEGIN\" i \"END\""

	awk '
		BEGIN {
			FS = ","
			print "Lista produktow:"
		}

		( NR > 1 && $1 != "" ) {
			print $1
		}
	' <<< cat "awk/prices.csv"	# "here string"
	pause

	awk '
		{
			sum += $1
		}
		END {
			printf "Suma: "sum
		}' <<LINEOFTEXT
	1
	2
	3
LINEOFTEXT
	pause
}

sample3() {
	title "Tablice"

	ls -lR /usr/local | awk '
		NF > 7 {
			files[$3]++
		}
		END {
			for (i in files) {
				printf "Pliki uzytkownika %s: %d\n", i, files[i]
			}
		}
	'
	pause
}

sample4() {
	title "Funkcje"

	wc -l *.sh | awk '
		function maxNumber(array)
		{
			result = 0;
			for (i in array) {
				if (array[i] > result) {
					result = array[i]
				}
			}

			return result
		}

		( $NF != "total" ) {
			lines[$2]=$1
		}

		END {
			max = maxNumber(lines)
			for (i in lines) {
				scaled = 30 * lines[i] / max
				printf "%-17s [%4d]: ", i, lines[i]

				while(scaled-- > 0) {
					printf "#"
				}
				printf "\n"
			}
		}
	'
	pause

	infonl "To samo za pomoca pliku zrodlowego:"
	wc -l *.sh | awk -f awk/sample4.awk
	pause

	infonl "To samo za pomoca pliku z parametrem +x:"
	wc -l *.sh | ./awk/sample4.awk
	pause
}

sample5() {
	title "Wyrazenia regularne i przekazywanie argumentow"

	ls /var/log/ | awk -v pattern="2\\.gz$" '
		/\.log$/ {
			printf "Default: "
			print $1
		}

		$0 ~ pattern {
			printf "User:    "
			print $1
		}
	' 2> /dev/null
}

sample1
sample2
sample3
sample4
sample5