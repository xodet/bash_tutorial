#!/bin/bash
. functions.sh

sample1() {
	title "Petle iteracyjne"

	for i in 1 2 3; do
		printf "$i,"
	done
	pausenl


	A="4 5"
	for i in $A; do
		printf "$i,"
	done
	pausenl


	for i in `seq 0 2`; do
		printf "$i,"
	done
	pausenl


	for i in `seq 0 10 100`; do
		printf "$i,"
	done
	pausenl


	for i in {a..f}; do
		printf "$i,"
	done
	pausenl


	for i in 1 $A {t..z} `seq 6 9`; do
		printf "$i,"
	done
	pausenl
}

sample2() {
	title "Inne rodzaje petli"

	A=0
	while [ $A -lt 3 ]; do
		printf "$A,"
		let A++
	done
	pausenl


	A=3
	until [ $A -eq 0 ]; do
		printf "$A,"
		let A--
	done
	pausenl


	for ((i=0; i<5; i++)) {
		printf "$i,"
	}
	pausenl
}

sample3() {
	title "Przerwania"

	A=`seq 0 4`
	for i in $A; do
		for j in $A; do
			printf "$j "
		done
		printf "\n"
	done
	pause

	
	for i in $A; do
		for j in $A; do
			if [ $j = 3 ]; then
				break 2;
			fi
			printf "$j "
		done
		printf "\n"
	done
	pausenl


	for i in $A; do
		for j in $A; do
			if [ $j = 3 ]; then
				continue 2;
			fi
			printf "$j "
		done
		printf "\n"
	done
	pausenl
}

sample1
sample2
sample3