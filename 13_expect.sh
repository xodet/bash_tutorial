#!/bin/bash
. functions.sh

sample1() {
	title "Prosty przyklad - generowanie klucza RSA"

	pasword=123456
	keyPath="`pwd`/expect/tmp_key"
	
	rm $keyPath $keyPath.pub &> /dev/null
	
	# wymagana biblioteka tcl-expect
	expect -c '
		spawn ssh-keygen -t rsa
		expect {
			"Enter file in which to save the key" {
				send "'$keyPath'\r"
				exp_continue
			}
			"Overwrite" {
				send "y\r"
				exp_continue
			}
			"Enter passphrase" {
				send "'$pasword'\r"
				exp_continue
			}
			"Enter same passphrase " {
				send "'$pasword'\r"
				exp_continue
			}
		}
	'
	pause

	infonl "Potwierdzenie istnienia pliku:"
	ls -l $keyPath
	pause
}

sample2() {
	title "Poprzedni przyklad za pomoca pliku wykonywalnego"

	./expect/script.exp "`pwd`/expect/tmp_key" 123456
	pause
}

sample1
sample2